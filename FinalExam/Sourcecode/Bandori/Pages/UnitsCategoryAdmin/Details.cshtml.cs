using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Bandori.Data;
using Bandori.Models;

namespace Bandori.Pages.UnitsCategoryAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly Bandori.Data.BandoriContext _context;

        public DetailsModel(Bandori.Data.BandoriContext context)
        {
            _context = context;
        }

        public UnitsCategory UnitsCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            UnitsCategory = await _context.UnitsCategory.FirstOrDefaultAsync(m => m.UnitsCategoryID == id);

            if (UnitsCategory == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
