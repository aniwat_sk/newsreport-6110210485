using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Bandori.Data;
using Bandori.Models;

namespace Bandori.Pages.UnitsCategoryAdmin
{
    public class CreateModel : PageModel
    {
        private readonly Bandori.Data.BandoriContext _context;

        public CreateModel(Bandori.Data.BandoriContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public UnitsCategory UnitsCategory { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.UnitsCategory.Add(UnitsCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}