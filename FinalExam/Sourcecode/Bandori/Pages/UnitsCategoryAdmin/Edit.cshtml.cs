using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Bandori.Data;
using Bandori.Models;

namespace Bandori.Pages.UnitsCategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly Bandori.Data.BandoriContext _context;

        public EditModel(Bandori.Data.BandoriContext context)
        {
            _context = context;
        }

        [BindProperty]
        public UnitsCategory UnitsCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            UnitsCategory = await _context.UnitsCategory.FirstOrDefaultAsync(m => m.UnitsCategoryID == id);

            if (UnitsCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(UnitsCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UnitsCategoryExists(UnitsCategory.UnitsCategoryID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool UnitsCategoryExists(int id)
        {
            return _context.UnitsCategory.Any(e => e.UnitsCategoryID == id);
        }
    }
}
