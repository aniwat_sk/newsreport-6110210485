using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Bandori.Data;
using Bandori.Models;

namespace Bandori.Pages.UnitsCategoryAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly Bandori.Data.BandoriContext _context;

        public DeleteModel(Bandori.Data.BandoriContext context)
        {
            _context = context;
        }

        [BindProperty]
        public UnitsCategory UnitsCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            UnitsCategory = await _context.UnitsCategory.FirstOrDefaultAsync(m => m.UnitsCategoryID == id);

            if (UnitsCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            UnitsCategory = await _context.UnitsCategory.FindAsync(id);

            if (UnitsCategory != null)
            {
                _context.UnitsCategory.Remove(UnitsCategory);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
