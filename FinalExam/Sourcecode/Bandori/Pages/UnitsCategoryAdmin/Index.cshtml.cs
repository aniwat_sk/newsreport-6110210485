using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Bandori.Data;
using Bandori.Models;

namespace Bandori.Pages.UnitsCategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly Bandori.Data.BandoriContext _context;

        public IndexModel(Bandori.Data.BandoriContext context)
        {
            _context = context;
        }

        public IList<UnitsCategory> UnitsCategory { get;set; }

        public async Task OnGetAsync()
        {
            UnitsCategory = await _context.UnitsCategory.ToListAsync();
        }
    }
}
