using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Bandori.Data;
using Bandori.Models;

namespace Bandori.Pages.UnitsAdmin
{
    public class IndexModel : PageModel
    {
        private readonly Bandori.Data.BandoriContext _context;

        public IndexModel(Bandori.Data.BandoriContext context)
        {
            _context = context;
        }

        public IList<Units> Units { get;set; }

        public async Task OnGetAsync()
        {
            Units = await _context.unitsList
                .Include(u => u.UnitsCat)
                .Include(u => u.postUser).ToListAsync();
        }
    }
}
