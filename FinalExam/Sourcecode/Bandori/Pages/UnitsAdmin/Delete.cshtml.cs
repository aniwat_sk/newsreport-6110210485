using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Bandori.Data;
using Bandori.Models;

namespace Bandori.Pages.UnitsAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly Bandori.Data.BandoriContext _context;

        public DeleteModel(Bandori.Data.BandoriContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Units Units { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Units = await _context.unitsList
                .Include(u => u.UnitsCat)
                .Include(u => u.postUser).FirstOrDefaultAsync(m => m.UnitsID == id);

            if (Units == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Units = await _context.unitsList.FindAsync(id);

            if (Units != null)
            {
                _context.unitsList.Remove(Units);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
