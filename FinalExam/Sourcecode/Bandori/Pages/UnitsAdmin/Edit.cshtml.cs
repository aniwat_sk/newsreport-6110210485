using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Bandori.Data;
using Bandori.Models;

namespace Bandori.Pages.UnitsAdmin
{
    public class EditModel : PageModel
    {
        private readonly Bandori.Data.BandoriContext _context;

        public EditModel(Bandori.Data.BandoriContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Units Units { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Units = await _context.unitsList
                .Include(u => u.UnitsCat)
                .Include(u => u.postUser).FirstOrDefaultAsync(m => m.UnitsID == id);

            if (Units == null)
            {
                return NotFound();
            }
           ViewData["UnitsCategoryID"] = new SelectList(_context.UnitsCategory, "UnitsCategoryID", "UnitsName");
           ViewData["BandoriUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Units).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UnitsExists(Units.UnitsID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool UnitsExists(int id)
        {
            return _context.unitsList.Any(e => e.UnitsID == id);
        }
    }
}
