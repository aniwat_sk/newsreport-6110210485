using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Bandori.Models
{
	public class BandoriUser : IdentityUser{
		public string FirstName { get; set;}
		public string LastName { get; set;}
	}

	public class UnitsCategory{
		public int UnitsCategoryID { get; set;}
		public string UnitsName {get; set;}
	}
	
	public class Units{
		public int UnitsID {get; set;}
		
		public int UnitsCategoryID { get; set; }
		public UnitsCategory UnitsCat {get; set;}
		
		[DataType(DataType.Date)]
		public string LiveDate {get; set;}
		public string UnitsDetail {get; set;}
		public string LiveDetail {get; set;}
		
		public string BandoriUserId {get; set;}
		public BandoriUser postUser {get; set;}
	}
}