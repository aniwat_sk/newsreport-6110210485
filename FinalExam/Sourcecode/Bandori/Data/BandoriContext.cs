using Bandori.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Bandori.Data
{
	public class BandoriContext : IdentityDbContext<BandoriUser>
	{
		public DbSet<Units> unitsList { get; set; }
		public DbSet<UnitsCategory> UnitsCategory { get; set; }
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite(@"Data source=Bandori.db");
		}
	}
}