using System;
using System.ComponentModel.DataAnnotations;

namespace NewsReport.Models
{
	public class NewsCategory{
		public int NewsCategoryID {get; set;}
		public string ShortName {get; set;}
		public string FullName {get; set;}
	}
	public class News{
		public int NewsID {get; set;}
		
		public int NewsCategoryID {get; set;}
		public NewsCategory NewsCat {get; set;}
		
		[DataType(DataType.Date)]
		public string ReportDate {get; set;}
		public string NewsDetail {get; set;}
		public float GpsLat {get; set;}
		public float GpsLng {get; set;}
		public string NewsStatus {get; set;}
		
	}
}